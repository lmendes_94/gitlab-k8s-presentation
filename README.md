# Deploy da infraestrutura até a aplicação com gitlab pipelines

## Agenda

* 1 - Introdução a Infraestruta como Código
    * 1.1 - O que é infraestrutura como código
    * 1.2 - Tipos de infraestrura como código
    * 1.3 - Ferramentas de infraestrutura como código

* 2 - Provisionando e configurando um cluster k8s com terraform e helm
    * 2.1 - Overview sobre a Arquitetura do kubernetes
    * 2.2 - Provisionando um cluster k8s com gitlab pipelines e terraform
    * 2.3 - Boas praticas na configuração de um cluster k8s

* 3 - Realizando a entrega de uma aplicação no kubernetes via gitlab
    * 3.1 - Fluxo de entrega da aplicação
    * 3.2 - Configuração da pipeline
    * 3.3 - Deployment da aplicação no k8s
    * 3.4 - Boas praticas no deployment HA de uma aplicação no cluster

## Introdução a infraestrutura como código

### O que é infraestrutura como código ?

A infraestrutura como código (IaC) permite o provisionamento e/ou a gestão de recursos de infraestrutura de forma `automatizada`. 

### Quais são as categorias de infraestrutura como código ?

* Ferramentas de provisionamento
* Ferramentas de gestão de configuração 

### Ferramentas de infraestrutura como código
* Ferramentas Open Source    
    * puppet
    * chef
    * ansible
    * terraform
    * pulumi

<br />

* Ferramentas proprietárias
    * aws cloudformation
    * azure resource manager (ARM) 
    * Bicep
    * Google Cloud Deployment Manager 

## Provisionando e configurando um cluster k8s com terraform e helm

### Overview Arquitetura Kubernetes

![arquitetura do k8s](./assets/kubernetes-architecture.png)

### Provisionando um cluster k8s com gitlab pipelines e terraform

![entrega do cluster](./assets/entrega-cluster.png)

#### Exemplo do manifesto de ci para deploy do kuberntes com terraform


```yaml
image: registry.gitlab.com/gitlab-org/terraform-images/stable:latest

variables:
  TF_ROOT: ${CI_PROJECT_DIR}
  TF_ADDRESS: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/${CI_PROJECT_NAME}

cache:
  key: example-production
  paths:
    - ${TF_ROOT}/.terraform

before_script:
  - cd ${TF_ROOT}

stages:
  - prepare
  - validate
  - build
  - deploy

init:
  stage: prepare
  script:
    # possibilidade de gestão da versão do terraform com tfenv
    - gitlab-terraform init
    - gitlab-terraform fmt -check -recursive

validate:
  stage: validate
  script:
    - gitlab-terraform validate
    # outros steps como linting, testing e validação de custos com infracost...

plan:
  stage: build
  script:
    - gitlab-terraform plan
    - gitlab-terraform plan-json
  artifacts:
    name: plan
    paths:
      - ${TF_ROOT}/plan.cache
    reports:
      terraform: ${TF_ROOT}/plan.json

# Possibilidade de gestão de states por ambientes utilizando terraform workspaces
apply:
  stage: deploy
  environment:
    name: production
  script:     
    - gitlab-terraform apply
  dependencies:
    - plan
  when: manual
  only:
    - master
```

#### Exemplo da configuração básica do terraform

```hcl
terraform {
    backend "http" {}

    required_version = ">=1.0.0"
    required_providers {
        aws        = ">= 3.56.0"
        kubernetes = ">= 1.11.1"
        helm       = ">= 2.2.0" 
    }
}

data "aws_eks_cluster" "eks" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "eks" {
  name = module.eks.cluster_id
}

provider "aws" {
    region = "us-east-1"
    assume_role {
        role_arn = "arn:aws:iam::123456789012:role/EksDeploymentRole"
    }
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.eks.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.eks.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.eks.token
}

provider "helm" {
    kubernetes {
        host                   = data.aws_eks_cluster.eks.endpoint
        cluster_ca_certificate = base64decode(data.aws_eks_cluster.eks.certificate_authority[0].data)
        token                  = data.aws_eks_cluster_auth.eks.token        
    }
}

module "eks" {
  source          = "terraform-aws-modules/eks/aws"

  cluster_version = "1.21"
  cluster_name    = "justsoftware-cluster"
  vpc_id          = "vpc-1234556abcdef"
  subnets         = ["subnet-abcde012", "subnet-bcde012a", "subnet-fghi345a"]

  worker_groups = [
    {
      instance_type = "m4.large"
      asg_max_size  = 5
    }
  ]
}

resource helm_release nginx_ingress {
  name       = "nginx-ingress-controller"

  repository = "https://charts.bitnami.com/bitnami"
  chart      = "nginx-ingress-controller"

  set {
    name  = "service.type"
    value = "ClusterIP"
  }
}
```

### Boas práticas ao provisionar o cluster k8s

1. trabalhar com HA no master-node (control-plane)
1. instalar peças para monitoramento do cluster (prometheus-stack)
1. definir estratégias de logging/tracing
1. instalar metrics-server
1. instalar ingress controller (aws_alb_controller, nginx, contour, traefik, envoy etc)
1. instalar cluster-autoscaler
1. controle de acesso via RBAC e configuração de politicas de network
1. instalar cert-manager para a gestão de certificados SSL

## Realizando a entrega de uma aplicação no kubernetes via gitlab

### Fluxo de entrega da aplicação

![entrega da aplicação](./assets/entrega-aplicacao.png)

### Configuração da pipeline

```yaml
image: docker:19.03.12

stages:
  - build
  - test
  - pack
  - deploy

services:
  - docker:dind

variables:
  URL_PREFIX_SUBDOMAIN: meudeploy

cache:
  paths:
    - node_modules/

before_script:
  - docker login -u gitlab-ci-token -p "$CI_BUILD_TOKEN" "$CI_REGISTRY"

build_app:
  stage: build
  script:
    - npm install
    - npm run lint

# no passo de teste podemos executar checks de quality gate no sonar alem de fazer testes # de carga/performance caso necessário
testing:
  stage: test
  script:
    - npm run test:ci && npm run check-coverage

pack_image_feature:
  stage: pack
  script:
    - docker build -t "$CI_REGISTRY_IMAGE:${CI_COMMIT_REF_SLUG}" .
    - docker push "$CI_REGISTRY_IMAGE:${CI_COMMIT_REF_SLUG}"
  except:
    - master

pack_image:
  stage: pack
  script:
    - docker build -t "$CI_REGISTRY_IMAGE:latest" .
    - docker push "$CI_REGISTRY_IMAGE:latest"
  only:
    - master
  
deploy_staging:
  stage: deploy
  image: python:latest
  script:
    - export DEPLOYMENT_NAMESPACE="meudeploy-$CI_ENVIRONMENT_SLUG"
    - export IMAGE_TAG="$CI_REGISTRY_IMAGE:${CI_COMMIT_REF_SLUG}"
    - export URL_PREFIX_SUBDOMAIN="$URL_PREFIX_SUBDOMAIN-stage"
    - envsubst < deploy.yaml.tpl > deployment.yaml
    - aws eks update-kubeconfig --name $CLUSTER_NAME --region us-east-1
    - kubectl apply -f deployment.yaml    
  environment:
    name: staging
    url: https://${URL_PREFIX_SUBDOMAIN}.justsoftware.com.br
  except:
    - master

deploy_master:
  stage: deploy
  image: python:latest
  script:
    - export DEPLOYMENT_NAMESPACE=meudeploy
    - export IMAGE_TAG="$CI_REGISTRY_IMAGE:latest"
    - envsubst < deploy.yaml.tpl > deployment.yaml
    - aws eks update-kubeconfig --name $CLUSTER_NAME --region us-east-1
    - kubectl apply -f deployment.yaml    
  environment:
    name: production
    url: https://${URL_PREFIX_SUBDOMAIN}.justsoftware.com.br
  when: manual
  only:
    - master  
```

### Deployment da aplicação no k8s

#### Exemplo do manifesto de deploy do kubernetes

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name:  poc-k8s-api
  labels:
    app: api
  namespace: ${DEPLOYMENT_NAMESPACE}
spec:
  replicas: 2
  minReadySeconds: 40
  strategy:
    rollingUpdate:
        maxSurge: 3
        maxUnavailable: 0
    type: RollingUpdate
  selector:
    matchLabels:
      app: api
  template:
    metadata:
      labels:
        app: api
    spec:
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: labelKey
                operator: In
                values:
                  - labelValue
      topologyKey: kubernetes.io/hostname      
      containers:
      - image:  ${CONTAINER_IMAGE}
        name:  poc-k8s-api
        imagePullPolicy: IfNotPresent
        resources:
          limits:
            cpu: 500m
            memory: 512Mi
          requests:
            cpu: 250m
            memory: 256Mi
        livenessProbe:
          failureThreshold: 3
          httpGet:
            path: /health
            port: 3001
          initialDelaySeconds: 120
          periodSeconds: 240
          successThreshold: 1
          timeoutSeconds: 240
        readinessProbe:
          failureThreshold: 6
          httpGet:
            path: /health
            port: 3001
            scheme: HTTP
          initialDelaySeconds: 30
          periodSeconds: 30
          successThreshold: 1
          timeoutSeconds: 120        
        envFrom:
          - configMapRef:
              name: poc-k8s-config
        env:
          - name: DATABASE_USERNAME
            valueFrom:
              secretKeyRef:
                name: poc-k8s-secrets
                key: DATABASE_USERNAME
          - name: DATABASE_PASSWORD
            valueFrom:
              secretKeyRef:
                name: poc-k8s-secrets
                key: DATABASE_PASSWORD
        ports:
        - containerPort:  3001
      restartPolicy: Always      

---
kind: Service
apiVersion: v1
metadata:
  name:  poc-k8s
  labels:
    app: api    
  namespace: ${DEPLOYMENT_NAMESPACE}
spec:
  selector:
    app: api
  type: ClusterIP
  ports:
  - port: 3001
    targetPort: 3001

---
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: poc-k8s-ingress
  annotations:
    kubernetes.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/backend-protocol: HTTP
    nginx.ingress.kubernetes.io/load-balance: ewma
    nginx.ingress.kubernetes.io/proxy-read-timeout: "60"
    nginx.ingress.kubernetes.io/proxy-send-timeout: "60"
    nginx.ingress.kubernetes.io/proxy-body-size: 1m
    nginx.ingress.kubernetes.io/proxy-buffer-size: 8k
  namespace: ${DEPLOYMENT_NAMESPACE}
spec:
  rules:
  - host: ${URL_PREFIX_SUBDOMAIN}.justsoftware.com.br
    http:
      paths:
      - path: /
        backend:
          serviceName: poc-k8s
          servicePort: 3001

---
apiVersion: autoscaling/v2beta2
kind: HorizontalPodAutoscaler
metadata:
  namespace: ${DEPLOYMENT_NAMESPACE}
  name: poc-k8s          
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: poc-k8s
  minReplicas: 2
  maxReplicas: 10
  metrics:
    - type: Resource
      resource:
        name: cpu
        target:
          type: Utilization
          averageUtilization: 50

---
apiVersion: policy/v1beta1
kind: PodDisruptionBudget
metadata:
  name: poc-k8s
  namespace: ${DEPLOYMENT_NAMESPACE}
spec:
  selector:
    matchLabels:
      app: api
  minAvailable: 50%
```

### Boas Práticas para realização de um deployment no kubernetes com HA

1. configurar readiness/liveness para saber se o pod que foi feito o rollout está pronto/disponível para lidar com as requests
1. configurar rollingUpdate como estratégia de deploy
1. definição de limites na quantidade de recursos que um pod deve consumir (requests/limits)
1. evitar outages de pod caso ocorra algum problema de disponibilidade com PDB's (PodDisruptionBudget)
1. criação de HPA (HorizontalPodAutoScaler)
1. trabalhar com valores sensíveis utilizando kubernetes secrets
1. melhorar a distribuição dos pods entre os nodes do cluster com affinity/podAntiAffinity